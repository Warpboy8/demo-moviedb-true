document.addEventListener("DOMContentLoaded", function () {


    //Bouton revenir en haut

    var btnHaut = document.getElementById('retour');

    btnHaut.onclick = function () {
        document.body.scrollTop = 0; // Pour Safari
        document.documentElement.scrollTop = 0; // Pour Chrome, Firefox, IE and Opera
    };





    var connexion = new MovieDB();

    if (location.pathname.search("fiche-film.html") > 0) {

        var params = (new URL(document.location)).searchParams;

        // console.log(params.get("id"));

        connexion.requeteInfoFilm(params.get("id"));
    }
    else {
        connexion.requeteFilmPopulaire();
    }

});


class MovieDB {

    constructor() {
        console.log("Parfait 2");

        this.APIKey = "32a38ab386439289fc08b4b12d46914c";

        this.lang = "fr-CA";

        this.baseURL = "https://api.themoviedb.org/3/";

        this.imgPath = "https://image.tmdb.org/t/p/";

        this.largeurAffiche = ["92", "154", "185", "342", "500", "780"];

        this.largeurTeteAffiche = ["45", "185"];

        this.totalFilm = 6;

        this.totalActeur = 6;
    }

    requeteFilmPopulaire() {


        var xhr = new XMLHttpRequest();


        xhr.addEventListener("readystatechange", this.retourFilmPopulaire.bind(this));

        //xhr.open("GET", "https://api.themoviedb.org/3/movie/popular?page=1&language=en-US&api_key=%3C%3Capi_key%3E%3E");
        xhr.open("GET", this.baseURL + "movie/popular?page=1&language=" + this.lang + "&api_key=" + this.APIKey);

        xhr.send();
    }


    retourFilmPopulaire(e) {

        var target = e.currentTarget;

        var data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText).results;

            console.log(data);

            this.afficheFilmPopulaire(data)
        }

    }


    afficheFilmPopulaire(data) {

        for (var i = 0; i < this.totalFilm; i++) {

            var unArticle = document.querySelector(".template>.film").cloneNode(true);

            unArticle.querySelector("h3").innerText = data[i].title;

            if (data[i].overview === "") {

                unArticle.querySelector("p.description").innerText = "Aucune description";
            }
            else {
                unArticle.querySelector("p.description").innerText = data[i].overview;
            }

            unArticle.querySelector("a").setAttribute("href", "fiche-film.html?id=" + data[i].id);

            unArticle.querySelector("img").setAttribute("src", this.imgPath + "w500" + data[i].poster_path);

            document.querySelector(".liste-films").appendChild(unArticle);

            console.log(data[5].title);

        }


    }


    requeteInfoFilm(movieId) {

        console.log(movieId);


        var xhr = new XMLHttpRequest();

        xhr.addEventListener("readystatechange", this.retourInfoFilm.bind(this));

        //xhr.open("GET", "https://api.themoviedb.org/3/movie/%7Bmovie_id%7D?language=en-US&api_key=%3C%3Capi_key%3E%3E");
        xhr.open("GET", this.baseURL + "movie/" + movieId + "?language=" + this.lang + "&api_key=" + this.APIKey);

        xhr.send();
    }


    retourInfoFilm(e) {

        var target = e.currentTarget;

        var data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText);

            console.log(data);

            this.afficheInfoFilm(data);
        }

    }


    afficheInfoFilm(data) {

        var unArticle = document.querySelector(".fiche-film");

        unArticle.querySelector("h1").innerText = data.title;

        if (data.overview === "") {

            unArticle.querySelector("p.description").innerText = "Sans description";
        }
        else {
            unArticle.querySelector("p.description").innerText = data.overview;
        }

        unArticle.querySelector("img").setAttribute("src", this.imgPath + "w500" + data.poster_path);

        this.requeteActeur(data.id);

    }

    requeteActeur(movieId) {

        var xhr = new XMLHttpRequest();

        xhr.addEventListener("readystatechange", this.retourActeurFilm.bind(this));

        //xhr.open("GET", "https://api.themoviedb.org/3/movie/%7Bmovie_id%7D/credits?api_key=%3C%3Capi_key%3E%3E");
        xhr.open("GET", this.baseURL + "movie/" + movieId + "/credits?&api_key=" + this.APIKey);

        xhr.send();
    }

    retourActeurFilm(e) {

        var target = e.currentTarget;

        var data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText);

            console.log(data);

            this.afficheActeurFilm(data);
        }

    }

    afficheActeurFilm(data) {

        for (var i = 0; i < this.totalActeur; i++) {

            var unArticle = document.querySelector(".liste-acteurs").cloneNode(true);

            unArticle.querySelector("h1").innerText = data[i].title;

            if (data[i].overview === "") {

                unArticle.querySelector("p.description").innerText = "Sans description";
            }
            else {
                unArticle.querySelector("p.description").innerText = data[i].overview;
            }

            unArticle.querySelector("a").setAttribute("href", "fiche-film.html?id=" + data[i].id);

            unArticle.querySelector("img").setAttribute("src", this.imgPath + "w500" + data[i].poster_path);

            document.querySelector(".liste-films").appendChild(unArticle);

            console.log(data[i].title);

        }


    }

}
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJzY3JpcHQuanMiXSwic291cmNlc0NvbnRlbnQiOlsiZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihcIkRPTUNvbnRlbnRMb2FkZWRcIiwgZnVuY3Rpb24gKCkge1xyXG5cclxuXHJcbiAgICAvL0JvdXRvbiByZXZlbmlyIGVuIGhhdXRcclxuXHJcbiAgICB2YXIgYnRuSGF1dCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdyZXRvdXInKTtcclxuXHJcbiAgICBidG5IYXV0Lm9uY2xpY2sgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgZG9jdW1lbnQuYm9keS5zY3JvbGxUb3AgPSAwOyAvLyBQb3VyIFNhZmFyaVxyXG4gICAgICAgIGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5zY3JvbGxUb3AgPSAwOyAvLyBQb3VyIENocm9tZSwgRmlyZWZveCwgSUUgYW5kIE9wZXJhXHJcbiAgICB9O1xyXG5cclxuXHJcblxyXG5cclxuXHJcbiAgICB2YXIgY29ubmV4aW9uID0gbmV3IE1vdmllREIoKTtcclxuXHJcbiAgICBpZiAobG9jYXRpb24ucGF0aG5hbWUuc2VhcmNoKFwiZmljaGUtZmlsbS5odG1sXCIpID4gMCkge1xyXG5cclxuICAgICAgICB2YXIgcGFyYW1zID0gKG5ldyBVUkwoZG9jdW1lbnQubG9jYXRpb24pKS5zZWFyY2hQYXJhbXM7XHJcblxyXG4gICAgICAgIC8vIGNvbnNvbGUubG9nKHBhcmFtcy5nZXQoXCJpZFwiKSk7XHJcblxyXG4gICAgICAgIGNvbm5leGlvbi5yZXF1ZXRlSW5mb0ZpbG0ocGFyYW1zLmdldChcImlkXCIpKTtcclxuICAgIH1cclxuICAgIGVsc2Uge1xyXG4gICAgICAgIGNvbm5leGlvbi5yZXF1ZXRlRmlsbVBvcHVsYWlyZSgpO1xyXG4gICAgfVxyXG5cclxufSk7XHJcblxyXG5cclxuY2xhc3MgTW92aWVEQiB7XHJcblxyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgY29uc29sZS5sb2coXCJQYXJmYWl0IDJcIik7XHJcblxyXG4gICAgICAgIHRoaXMuQVBJS2V5ID0gXCIzMmEzOGFiMzg2NDM5Mjg5ZmMwOGI0YjEyZDQ2OTE0Y1wiO1xyXG5cclxuICAgICAgICB0aGlzLmxhbmcgPSBcImZyLUNBXCI7XHJcblxyXG4gICAgICAgIHRoaXMuYmFzZVVSTCA9IFwiaHR0cHM6Ly9hcGkudGhlbW92aWVkYi5vcmcvMy9cIjtcclxuXHJcbiAgICAgICAgdGhpcy5pbWdQYXRoID0gXCJodHRwczovL2ltYWdlLnRtZGIub3JnL3QvcC9cIjtcclxuXHJcbiAgICAgICAgdGhpcy5sYXJnZXVyQWZmaWNoZSA9IFtcIjkyXCIsIFwiMTU0XCIsIFwiMTg1XCIsIFwiMzQyXCIsIFwiNTAwXCIsIFwiNzgwXCJdO1xyXG5cclxuICAgICAgICB0aGlzLmxhcmdldXJUZXRlQWZmaWNoZSA9IFtcIjQ1XCIsIFwiMTg1XCJdO1xyXG5cclxuICAgICAgICB0aGlzLnRvdGFsRmlsbSA9IDY7XHJcblxyXG4gICAgICAgIHRoaXMudG90YWxBY3RldXIgPSA2O1xyXG4gICAgfVxyXG5cclxuICAgIHJlcXVldGVGaWxtUG9wdWxhaXJlKCkge1xyXG5cclxuXHJcbiAgICAgICAgdmFyIHhociA9IG5ldyBYTUxIdHRwUmVxdWVzdCgpO1xyXG5cclxuXHJcbiAgICAgICAgeGhyLmFkZEV2ZW50TGlzdGVuZXIoXCJyZWFkeXN0YXRlY2hhbmdlXCIsIHRoaXMucmV0b3VyRmlsbVBvcHVsYWlyZS5iaW5kKHRoaXMpKTtcclxuXHJcbiAgICAgICAgLy94aHIub3BlbihcIkdFVFwiLCBcImh0dHBzOi8vYXBpLnRoZW1vdmllZGIub3JnLzMvbW92aWUvcG9wdWxhcj9wYWdlPTEmbGFuZ3VhZ2U9ZW4tVVMmYXBpX2tleT0lM0MlM0NhcGlfa2V5JTNFJTNFXCIpO1xyXG4gICAgICAgIHhoci5vcGVuKFwiR0VUXCIsIHRoaXMuYmFzZVVSTCArIFwibW92aWUvcG9wdWxhcj9wYWdlPTEmbGFuZ3VhZ2U9XCIgKyB0aGlzLmxhbmcgKyBcIiZhcGlfa2V5PVwiICsgdGhpcy5BUElLZXkpO1xyXG5cclxuICAgICAgICB4aHIuc2VuZCgpO1xyXG4gICAgfVxyXG5cclxuXHJcbiAgICByZXRvdXJGaWxtUG9wdWxhaXJlKGUpIHtcclxuXHJcbiAgICAgICAgdmFyIHRhcmdldCA9IGUuY3VycmVudFRhcmdldDtcclxuXHJcbiAgICAgICAgdmFyIGRhdGE7XHJcblxyXG4gICAgICAgIGlmICh0YXJnZXQucmVhZHlTdGF0ZSA9PT0gdGFyZ2V0LkRPTkUpIHtcclxuXHJcbiAgICAgICAgICAgIGRhdGEgPSBKU09OLnBhcnNlKHRhcmdldC5yZXNwb25zZVRleHQpLnJlc3VsdHM7XHJcblxyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhkYXRhKTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuYWZmaWNoZUZpbG1Qb3B1bGFpcmUoZGF0YSlcclxuICAgICAgICB9XHJcblxyXG4gICAgfVxyXG5cclxuXHJcbiAgICBhZmZpY2hlRmlsbVBvcHVsYWlyZShkYXRhKSB7XHJcblxyXG4gICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgdGhpcy50b3RhbEZpbG07IGkrKykge1xyXG5cclxuICAgICAgICAgICAgdmFyIHVuQXJ0aWNsZSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIudGVtcGxhdGU+LmZpbG1cIikuY2xvbmVOb2RlKHRydWUpO1xyXG5cclxuICAgICAgICAgICAgdW5BcnRpY2xlLnF1ZXJ5U2VsZWN0b3IoXCJoM1wiKS5pbm5lclRleHQgPSBkYXRhW2ldLnRpdGxlO1xyXG5cclxuICAgICAgICAgICAgaWYgKGRhdGFbaV0ub3ZlcnZpZXcgPT09IFwiXCIpIHtcclxuXHJcbiAgICAgICAgICAgICAgICB1bkFydGljbGUucXVlcnlTZWxlY3RvcihcInAuZGVzY3JpcHRpb25cIikuaW5uZXJUZXh0ID0gXCJBdWN1bmUgZGVzY3JpcHRpb25cIjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHVuQXJ0aWNsZS5xdWVyeVNlbGVjdG9yKFwicC5kZXNjcmlwdGlvblwiKS5pbm5lclRleHQgPSBkYXRhW2ldLm92ZXJ2aWV3O1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB1bkFydGljbGUucXVlcnlTZWxlY3RvcihcImFcIikuc2V0QXR0cmlidXRlKFwiaHJlZlwiLCBcImZpY2hlLWZpbG0uaHRtbD9pZD1cIiArIGRhdGFbaV0uaWQpO1xyXG5cclxuICAgICAgICAgICAgdW5BcnRpY2xlLnF1ZXJ5U2VsZWN0b3IoXCJpbWdcIikuc2V0QXR0cmlidXRlKFwic3JjXCIsIHRoaXMuaW1nUGF0aCArIFwidzUwMFwiICsgZGF0YVtpXS5wb3N0ZXJfcGF0aCk7XHJcblxyXG4gICAgICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiLmxpc3RlLWZpbG1zXCIpLmFwcGVuZENoaWxkKHVuQXJ0aWNsZSk7XHJcblxyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhkYXRhWzVdLnRpdGxlKTtcclxuXHJcbiAgICAgICAgfVxyXG5cclxuXHJcbiAgICB9XHJcblxyXG5cclxuICAgIHJlcXVldGVJbmZvRmlsbShtb3ZpZUlkKSB7XHJcblxyXG4gICAgICAgIGNvbnNvbGUubG9nKG1vdmllSWQpO1xyXG5cclxuXHJcbiAgICAgICAgdmFyIHhociA9IG5ldyBYTUxIdHRwUmVxdWVzdCgpO1xyXG5cclxuICAgICAgICB4aHIuYWRkRXZlbnRMaXN0ZW5lcihcInJlYWR5c3RhdGVjaGFuZ2VcIiwgdGhpcy5yZXRvdXJJbmZvRmlsbS5iaW5kKHRoaXMpKTtcclxuXHJcbiAgICAgICAgLy94aHIub3BlbihcIkdFVFwiLCBcImh0dHBzOi8vYXBpLnRoZW1vdmllZGIub3JnLzMvbW92aWUvJTdCbW92aWVfaWQlN0Q/bGFuZ3VhZ2U9ZW4tVVMmYXBpX2tleT0lM0MlM0NhcGlfa2V5JTNFJTNFXCIpO1xyXG4gICAgICAgIHhoci5vcGVuKFwiR0VUXCIsIHRoaXMuYmFzZVVSTCArIFwibW92aWUvXCIgKyBtb3ZpZUlkICsgXCI/bGFuZ3VhZ2U9XCIgKyB0aGlzLmxhbmcgKyBcIiZhcGlfa2V5PVwiICsgdGhpcy5BUElLZXkpO1xyXG5cclxuICAgICAgICB4aHIuc2VuZCgpO1xyXG4gICAgfVxyXG5cclxuXHJcbiAgICByZXRvdXJJbmZvRmlsbShlKSB7XHJcblxyXG4gICAgICAgIHZhciB0YXJnZXQgPSBlLmN1cnJlbnRUYXJnZXQ7XHJcblxyXG4gICAgICAgIHZhciBkYXRhO1xyXG5cclxuICAgICAgICBpZiAodGFyZ2V0LnJlYWR5U3RhdGUgPT09IHRhcmdldC5ET05FKSB7XHJcblxyXG4gICAgICAgICAgICBkYXRhID0gSlNPTi5wYXJzZSh0YXJnZXQucmVzcG9uc2VUZXh0KTtcclxuXHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKGRhdGEpO1xyXG5cclxuICAgICAgICAgICAgdGhpcy5hZmZpY2hlSW5mb0ZpbG0oZGF0YSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgIH1cclxuXHJcblxyXG4gICAgYWZmaWNoZUluZm9GaWxtKGRhdGEpIHtcclxuXHJcbiAgICAgICAgdmFyIHVuQXJ0aWNsZSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIuZmljaGUtZmlsbVwiKTtcclxuXHJcbiAgICAgICAgdW5BcnRpY2xlLnF1ZXJ5U2VsZWN0b3IoXCJoMVwiKS5pbm5lclRleHQgPSBkYXRhLnRpdGxlO1xyXG5cclxuICAgICAgICBpZiAoZGF0YS5vdmVydmlldyA9PT0gXCJcIikge1xyXG5cclxuICAgICAgICAgICAgdW5BcnRpY2xlLnF1ZXJ5U2VsZWN0b3IoXCJwLmRlc2NyaXB0aW9uXCIpLmlubmVyVGV4dCA9IFwiU2FucyBkZXNjcmlwdGlvblwiO1xyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgdW5BcnRpY2xlLnF1ZXJ5U2VsZWN0b3IoXCJwLmRlc2NyaXB0aW9uXCIpLmlubmVyVGV4dCA9IGRhdGEub3ZlcnZpZXc7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB1bkFydGljbGUucXVlcnlTZWxlY3RvcihcImltZ1wiKS5zZXRBdHRyaWJ1dGUoXCJzcmNcIiwgdGhpcy5pbWdQYXRoICsgXCJ3NTAwXCIgKyBkYXRhLnBvc3Rlcl9wYXRoKTtcclxuXHJcbiAgICAgICAgdGhpcy5yZXF1ZXRlQWN0ZXVyKGRhdGEuaWQpO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICByZXF1ZXRlQWN0ZXVyKG1vdmllSWQpIHtcclxuXHJcbiAgICAgICAgdmFyIHhociA9IG5ldyBYTUxIdHRwUmVxdWVzdCgpO1xyXG5cclxuICAgICAgICB4aHIuYWRkRXZlbnRMaXN0ZW5lcihcInJlYWR5c3RhdGVjaGFuZ2VcIiwgdGhpcy5yZXRvdXJBY3RldXJGaWxtLmJpbmQodGhpcykpO1xyXG5cclxuICAgICAgICAvL3hoci5vcGVuKFwiR0VUXCIsIFwiaHR0cHM6Ly9hcGkudGhlbW92aWVkYi5vcmcvMy9tb3ZpZS8lN0Jtb3ZpZV9pZCU3RC9jcmVkaXRzP2FwaV9rZXk9JTNDJTNDYXBpX2tleSUzRSUzRVwiKTtcclxuICAgICAgICB4aHIub3BlbihcIkdFVFwiLCB0aGlzLmJhc2VVUkwgKyBcIm1vdmllL1wiICsgbW92aWVJZCArIFwiL2NyZWRpdHM/JmFwaV9rZXk9XCIgKyB0aGlzLkFQSUtleSk7XHJcblxyXG4gICAgICAgIHhoci5zZW5kKCk7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0b3VyQWN0ZXVyRmlsbShlKSB7XHJcblxyXG4gICAgICAgIHZhciB0YXJnZXQgPSBlLmN1cnJlbnRUYXJnZXQ7XHJcblxyXG4gICAgICAgIHZhciBkYXRhO1xyXG5cclxuICAgICAgICBpZiAodGFyZ2V0LnJlYWR5U3RhdGUgPT09IHRhcmdldC5ET05FKSB7XHJcblxyXG4gICAgICAgICAgICBkYXRhID0gSlNPTi5wYXJzZSh0YXJnZXQucmVzcG9uc2VUZXh0KTtcclxuXHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKGRhdGEpO1xyXG5cclxuICAgICAgICAgICAgdGhpcy5hZmZpY2hlQWN0ZXVyRmlsbShkYXRhKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgfVxyXG5cclxuICAgIGFmZmljaGVBY3RldXJGaWxtKGRhdGEpIHtcclxuXHJcbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCB0aGlzLnRvdGFsQWN0ZXVyOyBpKyspIHtcclxuXHJcbiAgICAgICAgICAgIHZhciB1bkFydGljbGUgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiLmxpc3RlLWFjdGV1cnNcIikuY2xvbmVOb2RlKHRydWUpO1xyXG5cclxuICAgICAgICAgICAgdW5BcnRpY2xlLnF1ZXJ5U2VsZWN0b3IoXCJoMVwiKS5pbm5lclRleHQgPSBkYXRhW2ldLnRpdGxlO1xyXG5cclxuICAgICAgICAgICAgaWYgKGRhdGFbaV0ub3ZlcnZpZXcgPT09IFwiXCIpIHtcclxuXHJcbiAgICAgICAgICAgICAgICB1bkFydGljbGUucXVlcnlTZWxlY3RvcihcInAuZGVzY3JpcHRpb25cIikuaW5uZXJUZXh0ID0gXCJTYW5zIGRlc2NyaXB0aW9uXCI7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB1bkFydGljbGUucXVlcnlTZWxlY3RvcihcInAuZGVzY3JpcHRpb25cIikuaW5uZXJUZXh0ID0gZGF0YVtpXS5vdmVydmlldztcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgdW5BcnRpY2xlLnF1ZXJ5U2VsZWN0b3IoXCJhXCIpLnNldEF0dHJpYnV0ZShcImhyZWZcIiwgXCJmaWNoZS1maWxtLmh0bWw/aWQ9XCIgKyBkYXRhW2ldLmlkKTtcclxuXHJcbiAgICAgICAgICAgIHVuQXJ0aWNsZS5xdWVyeVNlbGVjdG9yKFwiaW1nXCIpLnNldEF0dHJpYnV0ZShcInNyY1wiLCB0aGlzLmltZ1BhdGggKyBcInc1MDBcIiArIGRhdGFbaV0ucG9zdGVyX3BhdGgpO1xyXG5cclxuICAgICAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIi5saXN0ZS1maWxtc1wiKS5hcHBlbmRDaGlsZCh1bkFydGljbGUpO1xyXG5cclxuICAgICAgICAgICAgY29uc29sZS5sb2coZGF0YVtpXS50aXRsZSk7XHJcblxyXG4gICAgICAgIH1cclxuXHJcblxyXG4gICAgfVxyXG5cclxufSJdLCJmaWxlIjoic2NyaXB0LmpzIn0=
