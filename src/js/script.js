document.addEventListener("DOMContentLoaded", function () {


    //Bouton revenir en haut

    var btnHaut = document.getElementById('retour');

    btnHaut.onclick = function () {
        document.body.scrollTop = 0; // Pour Safari
        document.documentElement.scrollTop = 0; // Pour Chrome, Firefox, IE and Opera
    };





    var connexion = new MovieDB();

    if (location.pathname.search("fiche-film.html") > 0) {

        var params = (new URL(document.location)).searchParams;

        // console.log(params.get("id"));

        connexion.requeteInfoFilm(params.get("id"));
    }
    else {
        connexion.requeteFilmPopulaire();
    }

});


class MovieDB {

    constructor() {
        console.log("Parfait 2");

        this.APIKey = "32a38ab386439289fc08b4b12d46914c";

        this.lang = "fr-CA";

        this.baseURL = "https://api.themoviedb.org/3/";

        this.imgPath = "https://image.tmdb.org/t/p/";

        this.largeurAffiche = ["92", "154", "185", "342", "500", "780"];

        this.largeurTeteAffiche = ["45", "185"];

        this.totalFilm = 6;

        this.totalActeur = 6;
    }

    requeteFilmPopulaire() {


        var xhr = new XMLHttpRequest();


        xhr.addEventListener("readystatechange", this.retourFilmPopulaire.bind(this));

        //xhr.open("GET", "https://api.themoviedb.org/3/movie/popular?page=1&language=en-US&api_key=%3C%3Capi_key%3E%3E");
        xhr.open("GET", this.baseURL + "movie/popular?page=1&language=" + this.lang + "&api_key=" + this.APIKey);

        xhr.send();
    }


    retourFilmPopulaire(e) {

        var target = e.currentTarget;

        var data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText).results;

            console.log(data);

            this.afficheFilmPopulaire(data)
        }

    }


    afficheFilmPopulaire(data) {

        for (var i = 0; i < this.totalFilm; i++) {

            var unArticle = document.querySelector(".template>.film").cloneNode(true);

            unArticle.querySelector("h3").innerText = data[i].title;

            if (data[i].overview === "") {

                unArticle.querySelector("p.description").innerText = "Aucune description";
            }
            else {
                unArticle.querySelector("p.description").innerText = data[i].overview;
            }

            unArticle.querySelector("a").setAttribute("href", "fiche-film.html?id=" + data[i].id);

            unArticle.querySelector("img").setAttribute("src", this.imgPath + "w500" + data[i].poster_path);

            document.querySelector(".liste-films").appendChild(unArticle);

            console.log(data[5].title);

        }


    }


    requeteInfoFilm(movieId) {

        console.log(movieId);


        var xhr = new XMLHttpRequest();

        xhr.addEventListener("readystatechange", this.retourInfoFilm.bind(this));

        //xhr.open("GET", "https://api.themoviedb.org/3/movie/%7Bmovie_id%7D?language=en-US&api_key=%3C%3Capi_key%3E%3E");
        xhr.open("GET", this.baseURL + "movie/" + movieId + "?language=" + this.lang + "&api_key=" + this.APIKey);

        xhr.send();
    }


    retourInfoFilm(e) {

        var target = e.currentTarget;

        var data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText);

            console.log(data);

            this.afficheInfoFilm(data);
        }

    }


    afficheInfoFilm(data) {

        var unArticle = document.querySelector(".fiche-film");

        unArticle.querySelector("h1").innerText = data.title;

        if (data.overview === "") {

            unArticle.querySelector("p.description").innerText = "Sans description";
        }
        else {
            unArticle.querySelector("p.description").innerText = data.overview;
        }

        unArticle.querySelector("img").setAttribute("src", this.imgPath + "w500" + data.poster_path);

        this.requeteActeur(data.id);

    }

    requeteActeur(movieId) {

        var xhr = new XMLHttpRequest();

        xhr.addEventListener("readystatechange", this.retourActeurFilm.bind(this));

        //xhr.open("GET", "https://api.themoviedb.org/3/movie/%7Bmovie_id%7D/credits?api_key=%3C%3Capi_key%3E%3E");
        xhr.open("GET", this.baseURL + "movie/" + movieId + "/credits?&api_key=" + this.APIKey);

        xhr.send();
    }

    retourActeurFilm(e) {

        var target = e.currentTarget;

        var data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText);

            console.log(data);

            this.afficheActeurFilm(data);
        }

    }

    afficheActeurFilm(data) {

        for (var i = 0; i < this.totalActeur; i++) {

            var unArticle = document.querySelector(".liste-acteurs").cloneNode(true);

            unArticle.querySelector("h1").innerText = data[i].title;

            if (data[i].overview === "") {

                unArticle.querySelector("p.description").innerText = "Sans description";
            }
            else {
                unArticle.querySelector("p.description").innerText = data[i].overview;
            }

            unArticle.querySelector("a").setAttribute("href", "fiche-film.html?id=" + data[i].id);

            unArticle.querySelector("img").setAttribute("src", this.imgPath + "w500" + data[i].poster_path);

            document.querySelector(".liste-films").appendChild(unArticle);

            console.log(data[i].title);

        }


    }

}